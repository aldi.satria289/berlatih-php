<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Huruf</title>
</head>
<body>
    <h1>Tukar Besar Kecil - GIT</h1>
    <?php
function tukar_besar_kecil($string){
//kode di sini
$katabaru = '';
$length = strlen($string);
for ($i=0 ; $i<$length ; $i++) {
    if ($string[$i] >= 'A' && $string[$i] <= 'Z') {
        $katabaru .= strtolower($string[$i]);
    } else if ($string[$i] >= 'a' && $string[$i] <= 'z') {
        $katabaru .= strtoupper($string[$i]);
    } else {
        $katabaru .=$string[$i];
    }
}
echo $string. " => ";
echo $katabaru."<br>";

}


// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>       
        
</body>
</html>