<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tentukan Nilai</title>
</head>
<body>
    <h1>Tentukan Nilai - GIT</h1>
<?php
    function tentukan_nilai($number)
    {
    //  kode disini
    $hasil = "";
    if ($number >= 85){
        $hasil = "Sangat baik";
    }else if ($number >= 75){
        $hasil = "Baik";
    }else if ($number >= 65){
        $hasil = "Cukup";
    }else if ($number <= 55){
        $hasil = "Kurang";
    }
    echo "Nilai anda ".$number. " Hasilnya ". $hasil."<br>";
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
?>

       
        
</body>
</html>